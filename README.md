# README #

This is an updated version of https://sourceforge.net/projects/pyspline.
A program for working up EXAFS data.
This code originates from https://github.com/DiamondLightSource/pyspline and has been edited very slightly so to run on Ubuntu 16.04

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Tested for Ubuntu 16.04

1- Download pyspline files following the link in the sidebar menu at https://bitbucket.org/michael_lloyd_baker/pyspline.
2- unzip into a chosen directory.
3- open a terminal in the download directory.
4- type: "python setup2.py install" or type: "sudo python setup2.py install"
5 - type pyspline in the terminal whenever you want to run pyspline.

Note: python 2.7 is required with modules numpy and PyQt4.


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact